# oz65c02

Cycle accurate NMOS 6502 and WDC 65c02 emulator in C++ or Rust based on https://c74project.com/
(Warning: almost correct, but still in development)

This is a header only C++ library that consists of 4 files:
- `cpu.hpp`
- `types.hpp`
- `microcode.hpp`
- `process_microcode.hpp`

Files `extra/N6502.hpp` and `extra/Wdc65c02.hpp` are only to see an equivalent of what is generated at compile time. They can be produced with `make generator`.

Rust project is in `rust/` folder.

## Requirements for C++
- make
- a C++20 compiler: tested with x86-64 gcc 12.2 and x86-64 clang 16.0.0

## Building tests
`make`

## Testing
Just type:
`make test`

## Having a look to an equivalent of the code generated for `cpu.hpp` by the compiler
Just type:
`make generate_cpp`

## Regenerate Rust files `Wdc65c02.rs` and `N6502.rs`
Just type:
`make generate_rust`

## To do
- fix STP and WAI
- fix some minor issues with interrupts: delay with 1 cycle nops and NMI during BRK
- add original 65c02 cpu type by removing new bit instrutions & WAI & STP
- ...

